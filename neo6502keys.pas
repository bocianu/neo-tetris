unit neo6502keys;
interface
uses neo6502;
{$i hid_keycodes.inc}

type TKey = record
    code:byte;
    shift:boolean;
    ctrl:boolean;
    alt:boolean;
end;

procedure ReadKeyboard(var ukey:TKey);
function IsKeyDown(keycode,modifier:byte):boolean;

implementation

function IsKeyDown(keycode,modifier:byte):boolean;
begin
    if NeoIsKeyPressed(keycode)=0 then exit(false);
    result := (NeoMessage.params[1] and modifier) = modifier;
end;

procedure ReadKeyboard(var ukey:TKey);
var b,m: byte;
begin
    ukey.code := 0;
    for b:=KEY_MIN_CODE to KEY_MAX_CODE do if NeoIsKeyPressed(b)<>0 then ukey.code := b;
    m := NeoMessage.params[1];
    ukey.shift := (m and MOD_SHIFT) = MOD_SHIFT;
    ukey.ctrl := (m and MOD_CTRL) = MOD_CTRL;
    ukey.alt := (m and MOD_LALT) = MOD_LALT;
end;


end.