Tetris game port for neo6502 microcomputer.

https://www.neo6502.com/

Written in Mad-Pascal.

How to run it? On emulator you can load it via command line:

```
neo.exe tetris.bin@5000 run@5000
```

If you want to run it on device, put it on your SD/USB storage and type in your basic prompt:

```
load "tetris.neo"
```
