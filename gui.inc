procedure DrawCounters;
begin
    NeoSetDefaults(0,0,1,1,0);
    NeoDrawRect(rcol+34,WELL_TOP-8,rcol+34+(6*6),WELL_TOP);
    NeoDrawRect(rcol+34,WELL_TOP+24,rcol+34+(6*6),WELL_TOP+32);
    NeoDrawRect(rcol+58,WELL_TOP+96,rcol+58+(6*2),WELL_TOP+104);
    NeoDrawRect(WELL_LEFT+54,WELL_TOP-18,WELL_LEFT+54+(6*4),WELL_TOP-10);

    NeoSetDefaults(0,21,0,1,0);
    s:=GetPaddedInt(topscore,8);
    NeoDrawString(rcol+22,WELL_TOP-8,s);

    s:=GetPaddedInt(score,8);
    NeoDrawString(rcol+22,WELL_TOP+24,s);

    s:=GetPaddedInt(level,2);
    NeoDrawString(rcol+58,WELL_TOP+96,s);

    s:=GetPaddedInt(totalLines,4);
    NeoDrawString(WELL_LEFT+54,WELL_TOP-18,s);
end;


procedure DrawSquare(x,y,tiletype,xoff,yoff:byte);overload;
var 
    tx,ty:word;
begin
    tx:=(x shl 3) + WELL_LEFT - xoff;
    ty:=(y shl 3) + WELL_TOP - yoff;
    NeoSetDefaults(0,tiletype+8,1,0,0);
    NeoDrawRect(tx,ty,tx+7,ty+7);
    //upper highlight    
    NeoSetDefaults(0,tiletype+16,0,0,0);
    NeoDrawLine(tx,ty,tx+7,ty);
    NeoDrawLine(tx,ty,tx,ty+7);
    //lower shade
    NeoSetDefaults(0,tiletype,0,0,0);
    NeoDrawLine(tx+7,ty+1,tx+7,ty+8);
    NeoDrawLine(tx+1,ty+7,tx+7,ty+7);
end;

procedure DrawSquare(x,y,tiletype:byte);overload;
begin
    DrawSquare(x,y,tiletype,0,0);
end;

procedure ClearSquare(x,y:byte);
var tx,ty:word;
begin
    tx:=x shl 3 + WELL_LEFT;
    ty:=y shl 3 + WELL_TOP;
    NeoSetDefaults(0,0,1,0,0);
    NeoDrawRect(tx,ty,tx+7,ty+7);
end;

procedure ClearNext;
begin
    NeoSetDefaults(0,0,1,0,0);
    NeoDrawRect(rcol+15,WELL_TOP+54,rcol+55,WELL_TOP+79);
end;

procedure DrawNext(xp,yp:byte);
var 
    x,y,i:byte;
    tile: byte;
    tilebody: array [0..0] of byte;
begin
    i:=0;
    tilebody := getTile(nextTile,0);
    for y:=0 to tiles_sizes[nextTile]-1 do
        for x:=0 to tiles_sizes[nextTile]-1 do
            begin
                tile := tilebody[i];
                if tile = 1 then 
                       DrawSquare(xp+x,yp+y,nextTile+1,nextXoff[nextTile],nextYoff[nextTile]);
                inc(i);
            end;
end;

procedure DrawCorners(x0,y0,x1,y1:word);
begin
    NeoDrawPixel(x0,y0);
    NeoDrawPixel(x0,y1);
    NeoDrawPixel(x1,y0);
    NeoDrawPixel(x1,y1);
end;

procedure Box(x0,y0,x1,y1:word;c1,c2:byte);
begin
    NeoSetDefaults(0,c1,0,0,0);
    NeoDrawRect(x0-2,y0-2,x1+2,y1+2);
    NeoSetDefaults(0,c2,0,0,0);
    NeoDrawRect(x0-3,y0-3,x1+3,y1+3);
    NeoSetDefaults(0,24,0,0,0);
    DrawCorners(x0-2,y0-2,x1+2,y1+2);
    DrawCorners(x0-3,y0-3,x1+3,y1+3);
    NeoSetDefaults(0,0,1,0,0);
    NeoDrawRect(x0,y0,x1,y1);
end;

procedure ShowModal(s:Tstring);
var s2:TString;
begin
    NeoSetDefaults(0,24,1,0,0);
    NeoDrawRect(WELL_LEFT+6,WELL_TOP+78,WELL_LEFT+(WELL_WIDTH*8)-7,WELL_TOP+102);
    Box(WELL_LEFT+8,WELL_TOP+80,WELL_LEFT+(WELL_WIDTH*8)-9,WELL_TOP+100,8,8);
    NeoSetDefaults(0,21,0,1,0);
    move(@s[11],@s2[1],10);
    SetLength(s,10);
    SetLength(s2,10);
    NeoDrawString(WELL_LEFT+11,WELL_TOP+82,s);
    NeoDrawString(WELL_LEFT+11,WELL_TOP+92,s2);
end;

procedure DrawImage(x,y,w,h,src:word);
var px,py:word;
    b:byte;
begin
    w := w + x -1;
    h := h + y -1;
    for py:= y to h do 
        for px:= x to w do begin
            b:= peek(src);
            NeoSetDefaults(0,b,1,1,0);
            NeoDrawPixel(px,py);
            Inc(src);
        end;
end;

procedure DrawBackground;
begin
    ClrScr;
    NeoSetDefaults(0,24,1,0,0);
    NeoDrawRect(0,0,320,240);
    unlz4(pointer(LOGO_ADDRESS),pointer(IMAGE_BUFFER));
    DrawImage(10,9,102,110,IMAGE_BUFFER);
    unlz4(pointer(SPIRAL_ADDRESS),pointer(IMAGE_BUFFER));
    DrawImage(228,152,76,75,IMAGE_BUFFER);
    NeoSetDefaults(0,8,0,1,0);
    s:='Keys: Arrows';
    NeoDrawString(9,150,s);
    s:='SPACE: down';
    NeoDrawString(9,160,s);
    s:='ENTER: drop';
    NeoDrawString(9,170,s);
    s:='or Controller';
    NeoDrawString(9,180,s);

    s:='Code: bocianu';
    NeoDrawString(9,208,s);
    s:=Concat('Version: ',VER);
    NeoDrawString(9,218,s);
end;

procedure DrawGui;
begin
    Box(WELL_LEFT,WELL_TOP,WELL_LEFT+(WELL_WIDTH*8)-1,WELL_TOP+(WELL_HEIGHT*8)-1,8,8);
    Box(WELL_LEFT,WELL_TOP-20,WELL_LEFT+(WELL_WIDTH*8)-1,WELL_TOP-10,0,8);
    
    Box(rcol,WELL_TOP-20,rcol+70,WELL_TOP+32,0,8);
    Box(rcol+15,WELL_TOP+44,rcol+55,WELL_TOP+80,8,8);
    Box(rcol,WELL_TOP+94,rcol+70,WELL_TOP+104,0,8);
    SetLength(s,0);
    NeoSetDefaults(0,21,1,1,0);
    s := 'Lines:' ;
    NeoDrawString(WELL_LEFT+2,WELL_TOP-18,s);
    s := 'Top:' ;
    NeoDrawString(rcol+2,WELL_TOP-18,s);
    s:='Score:';
    NeoDrawString(rcol+2,WELL_TOP+14,s);
    s:='NEXT';
    NeoDrawString(rcol+24,WELL_TOP+46,s);
    s:='Level:';
    NeoDrawString(rcol+2,WELL_TOP+96,s);
    DrawCounters;
end;

procedure UpdateStartingLevel(col:byte);
begin
    s := GetPaddedInt(startingLevel,2);
    s := Concat('Starting level: ',s);
    NeoSetDefaults(0,24,1,1,0);
    NeoDrawRect(104,130,116,137);
    NeoSetDefaults(0,col,0,1,0);
    NeoDrawString(9,130,s);
    Str(startingLevel,s);
end;

function readJoy:byte;
var b:byte;
begin
    result:=0;
    b := NeoGetJoy(0);
    if b and 1 <> 0 then result := KEY_LEFT; // left 
    if b and 2 <> 0 then result := KEY_RIGHT; // right
    if b and 4 <> 0 then result := KEY_UP; // up 
    if b and 8 <> 0 then result := KEY_DOWN; // down 
    if b and 16 <> 0 then result := KEY_SPACE; // butA
    if b and 32 <> 0 then result := KEY_ENTER; // butB
end;

procedure GetUserInput;
begin
    ReadKeyboard(key);
    if key.code = 0 then key.code := readJoy;

    if key.code = 0 then begin
        joyDelay := 0;
    end;

    if joyDelay = 0 then begin 
        if key.code <> 0 then begin 
            joyDelay := JOY_DELAY;
        end;
    end else begin  
        Dec(joyDelay);
        key.code := 0;
    end;
end;


procedure TitleScreen;
var f:byte;
begin
    f:=0;
    repeat 
        NeoWaitForVblank;    
        UpdateStartingLevel(((f shr 2) and 7) or 16);
        GetUserInput;
        if key.code = KEY_UP then begin
            if startingLevel=1 then startingLevel:=STARTING_LEVEL_MAX else dec(startingLevel);
        end;
        if key.code = KEY_DOWN then begin
            if startingLevel=STARTING_LEVEL_MAX then startingLevel:=1 else inc(startingLevel);
        end;
        inc(f)
    until (key.code=KEY_ENTER) or (key.code=KEY_SPACE);
    UpdateStartingLevel(24);

    level := startingLevel;
    lines2levelup := level * LEVEL_UP_TRESHOLD;

end;