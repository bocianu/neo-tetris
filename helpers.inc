
function TPred(rotation:byte): byte;
begin
    TPred := (rotation-1) and 3;
end;

function TSucc(rotation:byte): byte;
begin
    TSucc := (rotation+1) and 3;
end;

function Min(a,b:byte): byte;
begin
    if (a<b) then result:=a else result:=b;
end;

function GetPaddedInt(i:cardinal;p:byte):TString;
var tmp:TString;
    o:byte;
begin
    FillChar(@result[1],p,'0');
    SetLength(result,p);
    str(i,tmp);
    o:=p-Length(tmp);
    move(@tmp[1],@result[o+1],Length(tmp));
end;

procedure LoadTopScore;
begin
    NeoLoad('tetris.hsc',word(@topscore));
    if NeoMessage.error<>0 then topscore:=1000;
end;

procedure SaveTopScore;
begin
    NeoSave('tetris.hsc',word(@topscore),4);
end;
