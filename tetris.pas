program tetris;
uses crt,neo6502,lz4,neo6502keys;

const 
{$i const.inc}
{$i tiles.inc}
{$i palette.inc}

var 
    board: array[0..WELL_WIDTH-1,0..WELL_HEIGHT-1] of byte;
    score,topscore: cardinal;
    lines2levelup: byte;
    levelLines: byte;
    totalLines: word;
    level: byte;
    startingLevel: byte;
    nextTile,currentTile: byte;
    rotation: byte;
    tileX,tileY: byte;
    key:Tkey;
    fallcounter: byte;
    joydelay: byte;
    scoretable: array [0..3] of word = (40,100,300,1200);
    tilepool: array [0..6] of byte;
    poolsize: byte;
    s: TString;
    spawnTile: boolean;
    comboCounter:byte;
    crackCounter:byte;

{$i random.inc}
{$i helpers.inc}
{$i gui.inc}
{$i board.inc}

procedure InitNewGame;
begin
    score := 0;
    levelLines := 0;
    totalLines := 0;
    ClrBoard;
    DrawCounters;
    poolsize := 0;
    nextTile := TossTile;
    spawnTile := true;
    comboCounter := 0;
    crackCounter := 0;
end;

begin

    LoadPalette;
    startingLevel:=1;
    level:=1;
    joyDelay:=0;
    Randomize;
    InitNewGame;
    LoadTopScore;
    DrawBackground;
    DrawGui;
    ShowModal('Press Fire to Start ');

    repeat

        // title screen
        TitleScreen;
        InitNewGame;
        NeoSoundFx(0,0);
        DrawGui;

        // main game loop
        repeat

            // new tile
            if spawnTile then
                begin
                    ClearNext;
                    rotation := 0;
                    currentTile := nextTile;
                    tileX := (WELL_WIDTH - tiles_sizes[currentTile]) shr 1;
                    tileY := 0;
                    if currentTile = 1 then dec(tileY);
                    nextTile := TossTile;
                    DrawNext(17,8);
                    spawnTile:=false;
                end;

            // this formula controls game speed based on players level
            fallcounter := 65-(Min(level,15)*4);

            // main steering loop
            repeat
                DrawBlock(tileX,tileY,currentTile,rotation,1);
                NeoWaitForVblank;
                dec(fallcounter);
                GetUserInput;
                if key.code<>0 then
                    begin
                        DrawBlock(tileX,tileY,currentTile,rotation,0);
                        if (key.code=KEY_LEFT) and CanMoveBlock(tileX-1,tileY,currentTile,rotation) then tileX := tileX-1;
                        if (key.code=KEY_RIGHT) and CanMoveBlock(tileX+1,tileY,currentTile,rotation) then tileX := tileX+1;
                        if (key.code=KEY_DOWN) and CanMoveBlock(tileX,tileY,currentTile,TPred(rotation)) then rotation := TPred(rotation);
                        if (key.code=KEY_UP) and CanMoveBlock(tileX,tileY,currentTile,TSucc(rotation)) then rotation := TSucc(rotation);
                        if (key.code=KEY_ENTER) then FastFall; // fall to bottom
                        if (key.code=KEY_SPACE) then fallcounter := 0; // fall one row
                    end;
                if crackCounter>0 then begin
                    dec(crackCounter);
                    if crackCounter = 0 then NeoMute(0);
                end;           
            until fallcounter=0;

            // clear old position
            DrawBlock(tileX,tileY,currentTile,rotation,0);

            // check the floor
            if CanMoveBlock(tileX,tileY+1,currentTile,rotation) then
                tileY := tileY+1 // tile falls
            else
                begin // tile hits the ground
                    crackCounter := CRACK_SOUND_LENGTH;
                    NeoSoundFx(0,4);
                    DrawBlock(tileX,tileY,currentTile,rotation,1);
                    DrawBlock(tileX,tileY,currentTile,rotation,2);
                    CheckRows(tileY);
                    if tileY=0 then key.code := KEY_ESC // game over
                        else spawnTile:=true;
                end;

        until key.code = KEY_ESC;
        // game over
        NeoMute(0);
        NeoSoundFx(0,5);

        if score>topscore then
            begin
                topscore := score;
                SaveTopScore;
            end;

        DrawCounters;
        ShowModal('   GAME      OVER   ');
        Pause(100);

    until false;
end.

{$r resources.rc}
