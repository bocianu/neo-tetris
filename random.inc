procedure ShufflePool;
var t:cardinal;
    t0:byte absolute t;
    r,s,d:byte;
begin
    t:=NeoGetTimer;
    poolsize:=0;
    for r:=0 to 6 do tilepool[r]:=r;
    repeat
        r := ((Random(0) xor t0) mod 6)+1; // 1..6
        d := (poolsize + r) mod 7; // offset to replace with
        s := tilepool[poolsize]; // temporary swap variable
        tilepool[poolsize]:=tilepool[d];
        tilepool[d]:=s;
        inc(poolsize);
    until poolsize=7;
end;

function TossTile:byte;
begin
    if poolsize = 0 then ShufflePool;
    dec(poolsize);
    result:=tilepool[poolsize];
end;