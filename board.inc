procedure ClrBoard;
var x,y: byte;
begin
    for x:=0 to WELL_WIDTH-1 do
        for y:=0 to WELL_HEIGHT-1 do
            board[x,y] := 0;
end;



procedure DrawBlock(xp,yp,tiletype,rotation,option:byte);
var x,y,i,tile: byte;
    tilebody: array [0..0] of byte;
begin
    i := 0;
    tilebody := getTile(tiletype,rotation);
    for y:=0 to tiles_sizes[tiletype]-1 do
        for x:=0 to tiles_sizes[tiletype]-1 do
            begin
                tile := tilebody[i];
                if tile = 1 then 
                    case option of 
                        0: ClearSquare(xp+x,yp+y);
                        1: DrawSquare(xp+x,yp+y,tiletype+1);
                        2: board[x+xp,y+yp] := tiletype+1;
                    end;
                inc(i);
            end;
end;

function CanMoveBlock(xp,yp,tiletype,rotation:byte): boolean;
var x,y,i: byte;
    tile: byte;
    tilebody: array [0..0] of byte;
begin
    result := true;
    i := 0;
    tilebody := getTile(tiletype,rotation);
    if tileY>WELL_HEIGHT then begin // I tile type fix
        Inc(tileY);
        Inc(yp);
    end;
    for y:=0 to tiles_sizes[tiletype]-1 do
        for x:=0 to tiles_sizes[tiletype]-1 do
            begin
                tile := tilebody[i];
                if ( tile=1 )then
                    begin
                        if (byte(x+xp) > WELL_WIDTH-1 )then result := false;
                        if (byte(y+yp) > WELL_HEIGHT-1 )then result := false;
                        if result and ( board[x+xp,y+yp]<>0 ) then result := false;
                    end;  
                inc(i);
            end;  
end;

procedure DeleteRow(yr:byte);
var x,y,freelines: byte;
    freerow: boolean;
begin
    TextColor(0);

    for x:=0 to WELL_WIDTH-1 do
        begin
            ClearSquare(x,y);
        end;

    freelines := 0;
    for y:=yr downto 2 do 
        begin
            freerow := true;
            for x:=0 to WELL_WIDTH-1 do
                begin
                    board[x,y] := board[x,y-1];
                    if ( board[x,y]=0 ) then
                        ClearSquare(x,y)
                    else begin  
                        DrawSquare(x,y,board[x,y]);
                        freerow := false;
                    end;
                end;
            if freerow then begin
                Inc(freelines);
                if freelines>3 then exit;
            end;
        end;
end;

procedure CheckRows(yr:byte);
var x,y,sfx: byte;
    del: boolean;
    rows: array [0..3] of byte;
    i,linecount: byte;
    bonus:cardinal;
begin
    linecount := 0;
    for y:=yr to Min(yr+4, WELL_HEIGHT-1) do
        begin
            del := true;

            for x:=0 to WELL_WIDTH-1 do
               if ( board[x,y]=0 )then del := false;

            if del then
                begin
                    rows[linecount]:=y;
                    inc(linecount);
                end;
        end;

    // at least one line is filled
    if linecount>0 then begin
        
        // regular score
        bonus := scoretable[linecount-1] * level;
        crackCounter := 0;
        sfx := 23;
        
        // combo score
        if comboCounter>0 then begin
            bonus := bonus + (50 * comboCounter * level);
            sfx := 21;
        end;
        Inc(comboCounter);
        
        score := score + bonus;
        DrawCounters;
        NeoMute(0);
        NeoSoundFx(0,sfx);

        // increase line counters
        levelLines := levelLines + linecount;
        totalLines := totalLines + linecount;
    end else begin
        comboCounter := 0;
    end;

    // animate disapearing tiles
    for x:=(WELL_WIDTH div 2) to WELL_WIDTH-1 do begin
        for i:=1 to linecount do begin
            y:=rows[i-1];
            ClearSquare(x,y);
            ClearSquare(WELL_WIDTH-1-x,y);
        end;
        NeoWaitForVblank;
        NeoWaitForVblank;
    end;

    // fall down rows
    for i:=1 to linecount do begin
        y:=rows[i-1];
        DeleteRow(y);
        NeoWaitForVblank;
    end;
    
    // check if level goal reached
    if ( levelLines>=lines2levelup )then
        begin
            NeoSoundFx(0,3);
            levelLines := levelLines - lines2levelup;
            lines2levelup := LEVEL_UP_TRESHOLD;
            level := level + 1;
            DrawCounters;
        end;

    
end;

procedure FastFall;
begin

    while CanMoveBlock(tileX,tileY+1,currentTile,rotation) do begin
        DrawBlock(tileX,tileY,currentTile,rotation,0);
        tileY := tileY + 1;
        DrawBlock(tileX,tileY,currentTile,rotation,1);
        NeoWaitForVblank;
    end;
    fallcounter := 0;
end;
