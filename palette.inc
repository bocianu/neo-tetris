
const 
palette: array [0..74] of byte = (
    $00, $00, $00, $00, $00, $78, $00, $78, $78, $00, $78, $00,
    $78, $78, $00, $78, $50, $00, $78, $00, $00, $50, $00, $78,
    $60, $60, $60, $00, $00, $f0, $00, $f0, $f0, $00, $f0, $00,
    $f0, $f0, $00, $f0, $a0, $00, $f0, $00, $00, $a0, $00, $f0,
    $ff, $ff, $ff, $99, $99, $ff, $99, $ff, $ff, $99, $ff, $99,
    $ff, $ff, $99, $ff, $dd, $99, $ff, $99, $99, $dd, $99, $ff,
    $30, $30, $40
);

procedure LoadPalette;
var c:byte;
begin
    for c:=0 to 24 do NeoSetPalette(c,palette[c*3],palette[c*3+1],palette[c*3+2]);
end;

